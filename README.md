# Selección y reclutamiento de Desarrolladores
> Julio 2018

## Indicaciones:

A continuación se le presenta un caso de estudio, sobre el cual deberá planificar, modelar y desarrollar la aplicación que se le solicita. Aplicando Patrones de Diseño y Programación Orientada a Objetos.

## Caso: Recursos Humanos:
Se requiere llevar el control del personal de Recursos Humanos, en cuanto a:

1. Datos Personales
2. Educación
3. Idiomas
4. Puestos
4. Salarios y Descuentos
5. Contratos, deberá considerar dos tipos de contratos: Permanente y Consultoría.
6. Gestionar las planillas de los salarios mensuales de los empleados de forma automatizada
para todos los empleados, creando una salida para impresión de la misma.

**Se requiere:**

Poder gestionar los datos personales de los empleados así como la información de
educación, idiomas y puestos con sus correspondientes salarios y descuentos básicos ISSS,
AFP y Renta. De igual manera general las planillas de salarios.

## Entregables:

Se le pide diseñar la Base de Datos en MariaDB, y desarrollar una aplicación en Symfony, que
cumpla con los requerimientos mínimos expuestos, corriendo en GNU/Linux.

Deberá presentar el día Lunes 23.07.2017 a las 9:00 a.m. en la Dirección de Tecnologías de
Información de la Universidad de El Salvador.
