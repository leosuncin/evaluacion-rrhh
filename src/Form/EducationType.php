<?php

namespace App\Form;

use App\Entity\Education;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('level', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Ninguno'                    => 1,
                    'Educación básica'           => 2,
                    'Educación media'            => 3,
                    'Educación no universitaria' => 4,
                    'Educación universitaria'    => 5,
                    'Certificación'              => 6
                ],
                'label' => 'Nivel'
            ])
            ->add('grade', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Ninguno' => [
                        'No estudió'        => 1,
                        'Educación inicial' => 2
                    ],
                    'Educación básica' => [
                        'Primer grado'  => 3,
                        'Segundo grado' => 4,
                        'Tercer grado'  => 5,
                        'Cuarto grado'  => 6,
                        'Quinto grado'  => 7,
                        'Sexto grado'   => 8,
                        'Séptimo grado' => 9,
                        'Octavo grado'  => 10,
                        'Noveno grado'  => 11
                    ],
                    'Educación media' => [
                        'Primer año de bachillerato'       => 12,
                        'Segundo año de bachillerato'      => 13,
                        'Tercer año de bachillerato o más' => 14
                    ],
                    'Educación no universitaria' => [
                        'Primer año'  => 15,
                        'Segundo año' => 16,
                        'Tercer año'  => 17
                    ],
                    'Educación universitaria' => [
                        'Primer año'                         => 18,
                        'Segundo año'                        => 19,
                        'Tercer año'                         => 20,
                        'Cuarto año'                         => 21,
                        'Quinto año o más'                   => 22,
                        'Maestría/especialización/doctorado' => 23
                    ],
                    'Certificación' => [
                        'Incompleto' => 24,
                        'Completo'   => 25
                    ],
                ],
                'label' => 'Grado'
            ])
            ->add('title', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Titulo'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Education::class,
        ]);
    }
}
