<?php

namespace App\Form;

use App\Entity\Contract;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('salary', 'Symfony\Component\Form\Extension\Core\Type\NumberType', [
                'label' => 'Salario base'
            ])
            ->add('period', 'Symfony\Component\Form\Extension\Core\Type\DateIntervalType', [
                'with_years'  => false,
                'with_months' => false,
                'with_days'   => true,
                'with_hours'  => false,
                'placeholder' => 'Escoja cada cuantos dias',
                'input' => 'string',
                'label' => 'Periodicidad',
                'labels' => ['days' => 'Dias']
            ])
            ->add('startAt', 'Symfony\Component\Form\Extension\Core\Type\DateType', [
                'widget' => 'single_text',
                'label' => 'Fecha de inicio'
            ])
            ->add('endAt', 'Symfony\Component\Form\Extension\Core\Type\DateType', [
                'widget' => 'single_text',
                'required' => false,
                'label' => 'Fecha de fin'
            ])
            ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Permanente' => 1,
                    'Consultoría' => 2
                ],
                'label' => 'Tipo'
            ])
            ->add('employee', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                'class' => 'App\Entity\Employee',
                'choice_label' => function($employee) {
                    return $employee->getForenames() . ' ' . $employee->getSurnames();
                },
                'label' => 'Empleado'
            ])
            ->add('job', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                'class' => 'App\Entity\Position',
                'choice_label' => 'name',
                'label' => 'Trabajo'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contract::class,
        ]);
    }
}
