<?php

namespace App\Form;

use App\Entity\ContactInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phoneNumber', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Numero de teléfono'
            ])
            ->add('email', 'Symfony\Component\Form\Extension\Core\Type\EmailType', [
                'label' => 'Correo electrónico'
            ])
            ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Casa'    => 1,
                    'Trabajo' => 2,
                    'Otro'    => 3
                ],
                'label' => 'Tipo'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactInfo::class,
        ]);
    }
}
