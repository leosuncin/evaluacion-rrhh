<?php

namespace App\Form;

use App\Entity\EmergencyContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmergencyContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Nombre completo'
            ])
            ->add('relationship', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Parentesco'
            ])
            ->add('phoneNumber', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Numero de teléfono'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmergencyContact::class,
        ]);
    }
}
