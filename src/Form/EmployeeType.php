<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('forenames', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Nombres'
            ])
            ->add('surnames', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Apellidos'
            ])
            ->add('dui', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'DUI'
            ])
            ->add('nit', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'NIT'
            ])
            ->add('isss', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'ISSS'
            ])
            ->add('nup', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'NUP'
            ])
            ->add('birthdate', 'Symfony\Component\Form\Extension\Core\Type\BirthdayType', [
                'widget' => 'single_text',
                'label' => 'Fecha de nacimiento'
            ])
            ->add('contactInfos', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', [
                'entry_type' => 'App\Form\ContactInfoType',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ])
            ->add('addresses', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', [
                'entry_type' => 'App\Form\AddressType',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ])
            ->add('emergencyContacts', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', [
                'entry_type' => 'App\Form\EmergencyContactType',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ])
            ->add('educations', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', [
                'entry_type' => 'App\Form\EducationType',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ])
            ->add('languages', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', [
                'entry_type' => 'App\Form\LanguageType',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
