<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('department', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Ahuachapán'   => '01',
                    'Santa Ana'    => '02',
                    'Sonsonate'    => '03',
                    'Chalatenango' => '04',
                    'La Libertad'  => '05',
                    'San Salvador' => '06',
                    'Cuscatlán'    => '07',
                    'La Paz'       => '08',
                    'Cabañas'      => '09',
                    'San Vicente'  => '10',
                    'Usulután'     => '11',
                    'San Miguel'   => '12',
                    'Morazán'      => '13',
                    'La Unión'     => '14'
                ],
                'placeholder' => 'Seleccionar el departamento',
                'label' => 'Departamento'
            ])
            ->add('city', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Ahuachapán' => [
                        'Ahuachapán'             => '0101',
                        'Apaneca'                => '0102',
                        'Atiquizaya'             => '0103',
                        'Concepción de Ataco'    => '0104',
                        'El Refugio'             => '0105',
                        'Guaymango'              => '0106',
                        'Jujutla'                => '0107',
                        'San Francisco Menéndez' => '0108',
                        'San Lorenzo'            => '0109',
                        'San Pedro Puxtla'       => '0110',
                        'Tacuba'                 => '0111',
                        'Turin'                  => '0112'
                    ],
                    'Santa Ana' => [
                        'Candelaria de la Frontera' => '0201',
                        'Coatepeque'                => '0202',
                        'Chalchuapa'                => '0203',
                        'El Congo'                  => '0204',
                        'El Porvenir'               => '0205',
                        'Masahuat'                  => '0206',
                        'Metapán'                   => '0207',
                        'San Antonio Pajonal'       => '0208',
                        'San Sebastián Salitrillo'  => '0209',
                        'Santa Ana'                 => '0210',
                        'Santa Rosa Guachipilín'    => '0211',
                        'Santiago de la Frontera'   => '0212',
                        'Texistepeque'              => '0213'
                    ],
                    'Sonsonate' => [
                        'Acajutla'                => '0301',
                        'Armenia'                 => '0302',
                        'Caluco'                  => '0303',
                        'Cuisnahuat'              => '0304',
                        'Santa Isabel Ishuatán'   => '0305',
                        'Izalco'                  => '0306',
                        'Juayua'                  => '0307',
                        'Nahuizalco'              => '0308',
                        'Nahulingo'               => '0309',
                        'Salcoatitán'             => '0310',
                        'San Antonio del Monte'   => '0311',
                        'San Julían'              => '0312',
                        'Santa Catarina Masahuat' => '0313',
                        'Santo Domingo de Guzmán' => '0314',
                        'Sonsonate'               => '0315',
                        'Sonzacate'               => '0316'
                    ],
                    'Chalatenango' => [
                        'Agua Caliente'            => '0401',
                        'Arcatao'                  => '0402',
                        'Azaculpa'                 => '0403',
                        'Citalá'                   => '0404',
                        'Comalapa'                 => '0405',
                        'Concepción Quezaltepeque' => '0406',
                        'Chalatenango'             => '0407',
                        'Dulce Nombre de María'    => '0408',
                        'El Carrizal'              => '0409',
                        'El Paraíso'               => '0410',
                        'La Laguna'                => '0411',
                        'La Palma'                 => '0412',
                        'La Reina'                 => '0413',
                        'Las Vueltas'              => '0414',
                        'Nombre de Jesús'          => '0415',
                        'Nueva Concepción'         => '0416',
                        'Nueva Trinidad'           => '0417',
                        'Ojos de Agua'             => '0418',
                        'Potoníco'                 => '0419',
                        'San Antonio de la Cruz'   => '0420',
                        'San Antonio los Ranchos'  => '0421',
                        'San Fernando'             => '0422',
                        'San Francisco Lempa'      => '0423',
                        'San Francisco Morazán'    => '0424',
                        'San Ignacio'              => '0425',
                        'San Isidro Labador'       => '0426',
                        'Cancasque'                => '0427',
                        'Las Flores'               => '0428',
                        'San Luis del Carmen'      => '0429',
                        'San Miguel de Mercedes'   => '0430',
                        'San Rafael'               => '0431',
                        'Santa Rita'               => '0432',
                        'Tejutla'                  => '0433'
                    ],
                    'La Libertad' => [
                        'Antiguo Cuscatlán'   => '0501',
                        'Ciudad Arce'         => '0502',
                        'Colón'               => '0503',
                        'Comasagua'           => '0504',
                        'Chiltiupán'          => '0505',
                        'Huizucar'            => '0506',
                        'Jayaque'             => '0507',
                        'Jicalapa'            => '0508',
                        'La Libertad'         => '0509',
                        'Nuevo Cuscatlán'     => '0510',
                        'Santa Tecla'         => '0511',
                        'Quezaltepeque'       => '0512',
                        'Sacacoyo'            => '0513',
                        'San José Villanueva' => '0514',
                        'San Juan Opico'      => '0515',
                        'San Matías'          => '0516',
                        'San Pablo Tacachico' => '0517',
                        'Tamanique'           => '0518',
                        'Talnique'            => '0519',
                        'Teotepque'           => '0520',
                        'Tepecoyo'            => '0521',
                        'Zaragoza'            => '0522'
                    ],
                    'San Salvador' => [
                        'Aguilares'            => '0601',
                        'Apopa'                => '0602',
                        'Ayutuxtepeque'        => '0603',
                        'Cuscatancingo'        => '0604',
                        'El Paisnal'           => '0605',
                        'Guazapa'              => '0606',
                        'Ilopango'             => '0607',
                        'Mejicanos'            => '0608',
                        'Nejapa'               => '0609',
                        'Panchimalco'          => '0610',
                        'Rosario de Mora'      => '0611',
                        'San Marcos'           => '0612',
                        'San Martín'           => '0613',
                        'San Salvador'         => '0614',
                        'Santiago Texacuangos' => '0615',
                        'Santo Tomás'          => '0616',
                        'Soyapango'            => '0617',
                        'Tonacatepeque'        => '0618',
                        'Ciudad Delgado'       => '0619'
                    ],
                    'Cuscatlán' => [
                        'Candelaria'              => '0701',
                        'Cojutepeque'             => '0702',
                        'El Carmen'               => '0703',
                        'El Rosario'              => '0704',
                        'Monte San Juan'          => '0705',
                        'Oratorio de Concepción'  => '0706',
                        'San Bartolomé Perulapía' => '0707',
                        'San Cristóbal'           => '0708',
                        'San José Guayabal'       => '0709',
                        'San Pedro Perulapán'     => '0710',
                        'San Rafael Cedros'       => '0711',
                        'San Ramón'               => '0712',
                        'Santa Cruz Analquito'    => '0713',
                        'Santa Cruz Michapa'      => '0714',
                        'Suchitoto'               => '0715',
                        'Tenancingo'              => '0716'
                    ],
                    'La Paz' => [
                        'Cuyultitán'               => '0801',
                        'El Rosario'               => '0802',
                        'Jerusalén'                => '0803',
                        'Mercedes la Ceiba'        => '0804',
                        'Olocuilta'                => '0805',
                        'Paraiso de Osorio'        => '0806',
                        'San Antonio Masahuat'     => '0807',
                        'San Egmidio'              => '0808',
                        'San Francisco Chinameca'  => '0809',
                        'San Juan Nonualco'        => '0810',
                        'San Juan Talpa'           => '0811',
                        'San Juan Tepezontes'      => '0812',
                        'San Luis Talpa'           => '0813',
                        'San Miguel Tepezontes'    => '0814',
                        'San Pedro Masahuat'       => '0815',
                        'San Pedro Nonualco'       => '0816',
                        'San Rafael Obrajuelo'     => '0817',
                        'Santa María Ostuma'       => '0818',
                        'Santiago Nonualco0'       => '0819',
                        'Tapalhuaca'               => '0820',
                        'Zacatecoluca'             => '0821',
                        'San Luis de la Herradura' => '0822'
                    ],
                    'Cabañas' => [
                        'Cinquera'      => '0901',
                        'Guacotecti'    => '0902',
                        'Ilobasco'      => '0903',
                        'jutiapa'       => '0904',
                        'San Isidro'    => '0905',
                        'Sensuntepeque' => '0906',
                        'Tejutepeque'   => '0907',
                        'Victoria'      => '0908',
                        'Dolores'       => '0909'
                    ],
                    'San Vicente' => [
                        'Apasteque'              => '1001',
                        'Guadalupe'              => '1002',
                        'San Cayetano Istepeque' => '1003',
                        'Santa Clara'            => '1004',
                        'Santo Domingo'          => '1005',
                        'San Estebán Catarina'   => '1006',
                        'San Ildelfonso'         => '1007',
                        'San Lorenzo'            => '1008',
                        'San Sebastían'          => '1009',
                        'San Vicente'            => '1010',
                        'Tecoluca'               => '1011',
                        'Tepetitán'              => '1012',
                        'Verapaz'                => '1013'
                    ],
                    'Usulután' => [
                        'Alegría'              => '1101',
                        'Berlín'               => '1102',
                        'California'           => '1103',
                        'Concepción Batres'    => '1104',
                        'El Triunfo'           => '1105',
                        'Ereguayquín'          => '1106',
                        'Estanxuelas'          => '1107',
                        'Jiquilisco'           => '1108',
                        'Jucuapa'              => '1109',
                        'Jucuarán'             => '1110',
                        'Mercedes Umaña'       => '1111',
                        'Nueva Granada'        => '1112',
                        'Ozatlán'              => '1113',
                        'Puerto el Triunfo'    => '1114',
                        'San Agustín'          => '1115',
                        'San Buena Ventura'    => '1116',
                        'San Dionisio'         => '1117',
                        'Santa Elena'          => '1118',
                        'San Francisco Javier' => '1119',
                        'Santa María'          => '1120',
                        'Santiago de María'    => '1121',
                        'Tecapán'              => '1122',
                        'Usulután'             => '1123'
                    ],
                    'San Miguel' => [
                        'Carolina'               => '1201',
                        'Ciudad Barrios'         => '1202',
                        'Comacarán'              => '1203',
                        'Chapeltique'            => '1204',
                        'Chinameca'              => '1205',
                        'Chirilagua'             => '1206',
                        'El Tránsito'            => '1207',
                        'Lolotique'              => '1208',
                        'Moncuagua'              => '1209',
                        'Nueva Guadalupe'        => '1210',
                        'Muevo Edén de San Juan' => '1211',
                        'Quelepa'                => '1212',
                        'San Antonio'            => '1213',
                        'San Gerardo'            => '1214',
                        'San Jorge'              => '1215',
                        'San Luis de la Reina'   => '1216',
                        'San Miguel'             => '1217',
                        'San Rafael'             => '1218',
                        'Sesori'                 => '1219',
                        'Uluazapa'               => '1220'
                    ],
                    'Morazán' => [
                        'Arambala'               => '1301',
                        'Cacaopera'              => '1302',
                        'Corinto'                => '1303',
                        'Chilanga'               => '1304',
                        'Delicias de Concepción' => '1305',
                        'El Divisadero'          => '1306',
                        'El Rosario'             => '1307',
                        'Gualococti'             => '1308',
                        'Guatajiagua'            => '1309',
                        'Joateca'                => '1310',
                        'Jocoaiteque'            => '1311',
                        'Jocoro'                 => '1312',
                        'Lolotiquillo'           => '1313',
                        'Meanguera'              => '1314',
                        'Osicala'                => '1315',
                        'Perquín'                => '1316',
                        'San Carlos'             => '1317',
                        'San Fernando'           => '1318',
                        'San Francisco Gotera'   => '1319',
                        'San Isidro'             => '1320',
                        'San Simón'              => '1321',
                        'Sensembra'              => '1322',
                        'Sociedad'               => '1323',
                        'Torola'                 => '1324',
                        'Yamabal'                => '1325',
                        'Yoloaiquín'             => '1326'
                    ],
                    'La Unión' => [
                        'Anamoros'              => '1401',
                        'Bolívar'               => '1402',
                        'Concepción de Oriente' => '1403',
                        'Conchagua'             => '1404',
                        'El Carmen'             => '1405',
                        'El Sauce'              => '1406',
                        'Intipucá'              => '1407',
                        'La Unión'              => '1408',
                        'Lislique'              => '1409',
                        'Meanguera del Golfo'   => '1410',
                        'Nueva Esparta'         => '1411',
                        'Pasaquina'             => '1412',
                        'Poloros'               => '1413',
                        'San Alejo'             => '1414',
                        'San José'              => '1415',
                        'Santa Rosa de Lima'    => '1416',
                        'Yayantique'            => '1417',
                        'Yucuaiquín'            => '1418'
                    ]
                ],
                'placeholder' => 'Seleccionar la ciudad',
                'label' => 'Ciudad'
            ])
            ->add('line1', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Dirección 1'
            ])
            ->add('line2', 'Symfony\Component\Form\Extension\Core\Type\TextType', [
                'label' => 'Dirección 2'
            ])
            ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', [
                'choices' => [
                    'Casa'    => 1,
                    'Trabajo' => 2,
                    'Otro'    => 3
                ],
                'label' => 'Tipo'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
