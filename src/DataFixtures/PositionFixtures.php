<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Position;

class PositionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $programador = new Position();
        $programador->setName('Programador');
        $manager->persist($programador);

        $profesor = new Position();
        $profesor->setName('Profesor');
        $manager->persist($profesor);

        $manager->flush();
    }
}
