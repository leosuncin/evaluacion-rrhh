<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180723101722 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS position (
            id INT NOT NULL AUTO_INCREMENT,
            name VARCHAR(120) NOT NULL,
            PRIMARY KEY (id)
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS contract (
            id INT NOT NULL AUTO_INCREMENT,
            salary DECIMAL(9,2) NOT NULL,
            period VARCHAR(60) NOT NULL,
            start_at DATE NOT NULL,
            end_at DATE NULL,
            type INT NOT NULL,
            employee_id INT NOT NULL,
            job_id INT NOT NULL,
            PRIMARY KEY (id),
            UNIQUE INDEX uq_contract_employee_position_idx (employee_id, job_id),
            CONSTRAINT fk_contract_employee
                FOREIGN KEY (employee_id)
                REFERENCES employee (id)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT,
            CONSTRAINT fk_contract_position
                FOREIGN KEY (job_id)
                REFERENCES position (id)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS payroll (
            id INT NOT NULL AUTO_INCREMENT,
            salary DECIMAL(9,2) NOT NULL,
            discounts DECIMAL(9,2) NOT NULL,
            payment DECIMAL(9,2) NOT NULL,
            pay_date DATE NOT NULL,
            contract_id INT NOT NULL,
            PRIMARY KEY (id),
            INDEX fk_payroll_contract_idx (contract_id ASC),
            CONSTRAINT fk_payroll_contract
                FOREIGN KEY (contract_id)
                REFERENCES contract (id)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE payroll DROP FOREIGN KEY fk_payroll_contract');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY fk_contract_employee');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY fk_contract_position');
        $this->addSql('DROP TABLE payroll');
        $this->addSql('DROP TABLE contract');
        $this->addSql('DROP TABLE position');
    }
}
