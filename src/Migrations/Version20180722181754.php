<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180722181754 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS employee (
            id INT NOT NULL AUTO_INCREMENT,
            forenames VARCHAR(128) NOT NULL,
            surnames VARCHAR(128) NOT NULL,
            dui VARCHAR(9) NOT NULL,
            nit VARCHAR(14) NULL,
            isss VARCHAR(9) NULL,
            nup VARCHAR(12) NULL,
            birthdate DATE NOT NULL,
            PRIMARY KEY (id)
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS contact_info (
            id INT NOT NULL AUTO_INCREMENT,
            phone_number VARCHAR(11) NOT NULL,
            email VARCHAR(60) NULL,
            type INT NOT NULL DEFAULT 1,
            employee_id INT NOT NULL,
            PRIMARY KEY (id),
            INDEX fk_contact_info_employee_idx (employee_id ASC),
            CONSTRAINT fk_contact_info_employee
                FOREIGN KEY (employee_id)
                REFERENCES employee (id)
                ON DELETE CASCADE
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS address (
            id INT NOT NULL AUTO_INCREMENT,
            department VARCHAR(2) NOT NULL,
            city VARCHAR(4) NOT NULL,
            line1 TINYTEXT NOT NULL,
            line2 TINYTEXT NULL,
            type INT NOT NULL DEFAULT 1,
            employee_id INT NOT NULL,
            PRIMARY KEY (id),
            INDEX fk_address_employee_idx (employee_id ASC),
            CONSTRAINT fk_address_employee
                FOREIGN KEY (employee_id)
                REFERENCES employee (id)
                ON DELETE CASCADE
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS emergency_contact (
            id INT NOT NULL AUTO_INCREMENT,
            full_name VARCHAR(256) NOT NULL,
            relationship VARCHAR(100) NOT NULL,
            phone_number VARCHAR(11) NOT NULL,
            employee_id INT NOT NULL,
            PRIMARY KEY (id),
            INDEX fk_emergency_contact_employee_idx (employee_id ASC),
            CONSTRAINT fk_emergency_contact_employee
                FOREIGN KEY (employee_id)
                REFERENCES employee (id)
                ON DELETE CASCADE
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS education (
            id INT NOT NULL AUTO_INCREMENT,
            level INT NOT NULL DEFAULT 1,
            grade INT NOT NULL DEFAULT 1,
            title VARCHAR(120) NOT NULL,
            employee_id INT NOT NULL,
            PRIMARY KEY (id),
            INDEX fk_education_employee_idx (employee_id ASC),
            CONSTRAINT fk_education_employee
                FOREIGN KEY (employee_id)
                REFERENCES employee (id)
                ON DELETE CASCADE
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS language (
            id INT NOT NULL AUTO_INCREMENT,
            lang VARCHAR(5) NOT NULL,
            level INT NOT NULL,
            employee_id INT NOT NULL,
            PRIMARY KEY (id),
            INDEX fk_language_employee_idx (employee_id ASC),
            CONSTRAINT fk_language_employee
                FOREIGN KEY (employee_id)
                REFERENCES employee (id)
                ON DELETE CASCADE
                ON UPDATE RESTRICT
        ) ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_info DROP FOREIGN KEY fk_contact_info_employee');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY fk_address_employee');
        $this->addSql('ALTER TABLE emergency_contact DROP FOREIGN KEY fk_emergency_contact_employee');
        $this->addSql('ALTER TABLE education DROP FOREIGN KEY fk_education_employee');
        $this->addSql('ALTER TABLE language DROP FOREIGN KEY fk_language_employee');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE education');
        $this->addSql('DROP TABLE contact_info');
        $this->addSql('DROP TABLE emergency_contact');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE employee');
    }
}
