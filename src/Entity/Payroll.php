<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayrollRepository")
 */
class Payroll
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $salary;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $discounts;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $payment;

    /**
     * @ORM\Column(type="date")
     */
    private $payDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contract")
     * @ORM\JoinColumn(nullable=false)
     */
    private $contract;

    public function getId()
    {
        return $this->id;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function setSalary($salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getDiscounts()
    {
        return $this->discounts;
    }

    public function setDiscounts($discounts): self
    {
        $this->discounts = $discounts;

        return $this;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function setPayment($payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPayDate(): ?\DateTimeInterface
    {
        return $this->payDate;
    }

    public function setPayDate(\DateTimeInterface $payDate): self
    {
        $this->payDate = $payDate;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }
}
