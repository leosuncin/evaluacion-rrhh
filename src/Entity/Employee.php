<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $forenames;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $surnames;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $dui;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $nit;

    /**
     * @ORM\Column(type="string", length=9, nullable=true)
     */
    private $isss;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $nup;

    /**
     * @ORM\Column(type="date")
     */
    private $birthdate;

    /**
     * @ORM\OneToMany(
     *   targetEntity="App\Entity\ContactInfo",
     *   mappedBy="employee",
     *   orphanRemoval=true,
     *   fetch="EXTRA_LAZY",
     *   cascade={"persist", "remove"}
     * )
     */
    private $contactInfos;

    /**
     * @ORM\OneToMany(
     *   targetEntity="App\Entity\Address",
     *   mappedBy="employee",
     *   orphanRemoval=true,
     *   fetch="EXTRA_LAZY",
     *   cascade={"persist", "remove"}
     * )
     */
    private $addresses;

    /**
     * @ORM\OneToMany(
     *   targetEntity="App\Entity\EmergencyContact",
     *   mappedBy="employee",
     *   orphanRemoval=true,
     *   fetch="EXTRA_LAZY",
     *   cascade={"persist", "remove"}
     * )
     */
    private $emergencyContacts;

    /**
     * @ORM\OneToMany(
     *   targetEntity="App\Entity\Education",
     *   mappedBy="employee",
     *   orphanRemoval=true,
     *   fetch="EXTRA_LAZY",
     *   cascade={"persist", "remove"}
     * )
     */
    private $educations;

    /**
     * @ORM\OneToMany(
     *   targetEntity="App\Entity\Language",
     *   mappedBy="employee",
     *   orphanRemoval=true,
     *   fetch="EXTRA_LAZY",
     *   cascade={"persist", "remove"}
     * )
     */
    private $languages;

    public function __construct()
    {
        $this->contactInfos = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->emergencyContacts = new ArrayCollection();
        $this->educations = new ArrayCollection();
        $this->languages = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getForenames(): ?string
    {
        return $this->forenames;
    }

    public function setForenames(string $forenames): self
    {
        $this->forenames = $forenames;

        return $this;
    }

    public function getSurnames(): ?string
    {
        return $this->surnames;
    }

    public function setSurnames(string $surnames): self
    {
        $this->surnames = $surnames;

        return $this;
    }

    public function getDui(): ?string
    {
        return $this->dui;
    }

    public function setDui(string $dui): self
    {
        $this->dui = $dui;

        return $this;
    }

    public function getNit(): ?string
    {
        return $this->nit;
    }

    public function setNit(?string $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    public function getIsss(): ?string
    {
        return $this->isss;
    }

    public function setIsss(?string $isss): self
    {
        $this->isss = $isss;

        return $this;
    }

    public function getNup(): ?string
    {
        return $this->nup;
    }

    public function setNup(?string $nup): self
    {
        $this->nup = $nup;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @return Collection|ContactInfo[]
     */
    public function getContactInfos(): Collection
    {
        return $this->contactInfos;
    }

    public function addContactInfo(ContactInfo $contactInfo): self
    {
        if (!$this->contactInfos->contains($contactInfo)) {
            $this->contactInfos[] = $contactInfo;
            $contactInfo->setEmployee($this);
        }

        return $this;
    }

    public function removeContactInfo(ContactInfo $contactInfo): self
    {
        if ($this->contactInfos->contains($contactInfo)) {
            $this->contactInfos->removeElement($contactInfo);
            // set the owning side to null (unless already changed)
            if ($contactInfo->getEmployee() === $this) {
                $contactInfo->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setEmployee($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getEmployee() === $this) {
                $address->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmergencyContact[]
     */
    public function getEmergencyContacts(): Collection
    {
        return $this->emergencyContacts;
    }

    public function addEmergencyContact(EmergencyContact $emergencyContact): self
    {
        if (!$this->emergencyContacts->contains($emergencyContact)) {
            $this->emergencyContacts[] = $emergencyContact;
            $emergencyContact->setEmployee($this);
        }

        return $this;
    }

    public function removeEmergencyContact(EmergencyContact $emergencyContact): self
    {
        if ($this->emergencyContacts->contains($emergencyContact)) {
            $this->emergencyContacts->removeElement($emergencyContact);
            // set the owning side to null (unless already changed)
            if ($emergencyContact->getEmployee() === $this) {
                $emergencyContact->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Education[]
     */
    public function getEducations(): Collection
    {
        return $this->educations;
    }

    public function addEducation(Education $education): self
    {
        if (!$this->educations->contains($education)) {
            $this->educations[] = $education;
            $education->setEmployee($this);
        }

        return $this;
    }

    public function removeEducation(Education $education): self
    {
        if ($this->educations->contains($education)) {
            $this->educations->removeElement($education);
            // set the owning side to null (unless already changed)
            if ($education->getEmployee() === $this) {
                $education->setEmployee(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Language[]
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    public function addLanguage(Language $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
            $language->setEmployee($this);
        }

        return $this;
    }

    public function removeLanguage(Language $language): self
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
            // set the owning side to null (unless already changed)
            if ($language->getEmployee() === $this) {
                $language->setEmployee(null);
            }
        }

        return $this;
    }
}
