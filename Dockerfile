FROM composer as composer
WORKDIR /usr/src/symfony
COPY composer.* ./
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN composer install --no-progress -o --no-dev --no-scripts -n

FROM node:10.7.0-alpine as nodejs
WORKDIR /usr/src/symfony
COPY . .
RUN npm set progress=false &&\
  npm config set depth 0 &&\
  npm config set loglevel error &&\
  npm ci || npm install &&\
  npx encore production

FROM phppm/nginx:1.0.1-2
RUN apk add --no-cache php7-iconv
COPY --from=composer /usr/src/symfony/vendor /var/www/vendor
COPY --from=nodejs /usr/src/symfony/public/build /var/www/public/build
