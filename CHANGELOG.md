<a name="0.9.1"></a>
## [0.9.1](https://gitlab.com/leosuncin/evaluacion-rrhh/compare/v0.9.0...v0.9.1) (2018-08-01)


### Features

* Agrega la pagina de inicio ([e4ceeec](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/e4ceeec))



<a name="0.9.0"></a>
# [v0.9.0](https://gitlab.com/leosuncin/evaluacion-rrhh/tree/v0.9.0) (2018-07-23)


### Bug Fixes

* Corrige error de sintaxis de SQL en versiones anteriores de MariaDB/MySQL ([3f0e60c](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/3f0e60c))


### Features

* Actualiza el CRUD de empleado ([4022b54](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/4022b54))
* Actualiza el script de la BD y agrega el archivo de migration de Doctrine ([1107c08](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/1107c08))
* Agrega el CRUD de contrato ([b253663](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/b253663))
* Agrega el CRUD para la informacion del empleado ([a9b4506](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/a9b4506))
* Agrega el resto de entidades para manejar la planilla ([80df22e](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/80df22e))
* Agrega la entidades relacionadas a empleado ([81428d7](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/81428d7))
* Agrega la primera version de la BD ([d5051a5](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/d5051a5))
* Crea filtros personalizados en Twig ([03c648a](https://gitlab.com/leosuncin/evaluacion-rrhh/commit/03c648a))



